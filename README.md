![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

Example [GitBook] website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Adding Content](#adding-content)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```yaml
# requiring the environment of NodeJS 10
image: node:10

# add 'node_modules' to cache for speeding up builds
cache:
  paths:
    - node_modules/ # Node modules and dependencies

before_script:
  - npm install gitbook-cli -g # install gitbook
  - gitbook fetch 3.2.3 # fetch final stable version
  - gitbook install # add any requested plugins in book.json

test:
  stage: test
  script:
    - gitbook build . public # build to public path
  only:
    - branches # this job will affect every branch except 'master'
  except:
    - master
    
# the 'pages' job will deploy and build your site to the 'public' path
pages:
  stage: deploy
  script:
    - gitbook build . public # build to public path
  artifacts:
    paths:
      - public
    expire_in: 1 week
  only:
    - master # this job will affect only the 'master' branch
```
## Adding Content
Adding content is pretty straight foreward in GitBook. You can add 
markdown files in the root folder itself or create folders and sub 
folders to organize the content. In order make the added file appear 
in the side bar, you need to add the file link as a list item to the 
[SUMMARY.md](SUMMARY.md) file. So if you've added a file named 
`example.md`, you need to add the file link in the SUMMARY.md file 
as shown below.

```md
* [Introduction](README.md)
* [<your title>](example.md)

```

If you have arranged your content in terms of folders and subfolders and 
wish the same to be reflected in the sidebar, you need to construct your 
list in the SUMMARY.md file in the same manner as that of your folder structure. 
For example if you've got three folders Alice, Bob and Charlie and they 
all have four files, i.e. `example1.md`, `example2.md`, `example3.md`, 
and `intro.md` in them and you want that structure preseved in the side menu, 
you may construct your list in the [SUMMARY.md](SUMMARY.md) file as follows:

```md
* [Introduction](README.md)
* [Alice](alice/intro.md)
  * [First Example](alice/example1.md)
  * [Second Example](alice/example2.md)
  * [Third Example](alice/example3.md)
* [Bob](bob/intro.md)
  * [First Example](bob/example1.md)
  * [Second Example](bob/example2.md)
  * [Third Example](bob/example3.md)
* [Charlie](charlie/intro.md)
  * [First Example](charlie/example1.md)
  * [Second Example](charlieexample2.md)
  * [Third Example](charlie/example3.md)
```
## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] GitBook `npm install gitbook-cli -g`
1. Fetch GitBook's latest stable version `gitbook fetch latest`
1. Preview your project: `gitbook serve`
1. Add content
1. Generate the website: `gitbook build` (optional)
1. Push your changes to the master branch: `git push`

Read more at GitBook's [documentation][].

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

----

Forked from @virtuacreative

[ci]: https://about.gitlab.com/gitlab-ci/
[GitBook]: https://www.gitbook.com/
[host the book]: https://gitlab.com/pages/gitbook/tree/pages
[install]: http://toolchain.gitbook.com/setup.html
[documentation]: http://toolchain.gitbook.com
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
